function createCard(name, description, pictureUrl, location, starts, ends) {
  return `
    <div class="card mb-3 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
      </div>
    </div>
  `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw "Invalid response";
    } else {
      const data = await response.json();

      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const location = details.conference.location.name;
          const pictureUrl = details.conference.location.picture_url;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const html = createCard(
            title,
            description,
            pictureUrl,
            location,
            starts,
            ends
          );
          const column = document.querySelector(`#col-${index % 3}`);
          column.innerHTML += html;
          console.log(details);
          index += 1;
        }
      }
    }
  } catch (e) {
    return "Error: " + e + " has occured";
  }
});

// const data = await response.json();

//       const conference = data.conferences[0];
//       const nameTag = document.querySelector(".card-title");
//       nameTag.innerHTML = conference.name;

//       const detailUrl = `http://localhost:8000${conference.href}`;
//       const detailResponse = await fetch(detailUrl);

//       if (detailResponse.ok) {
//         const details = await detailResponse.json();
//         const conferenceDescription = details.conference.description;
//         const descriptionTag = document.querySelector(".card-text");
//         descriptionTag.innerHTML = conferenceDescription;

//         const locationImage = details.conference.location.picture_url;
//         const imageTag = document.querySelector(".card-img-top");
//         imageTag.src = locationImage;
//       }
